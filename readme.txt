# Celestial Clock
---
by Sun Wang Jun A0110546R


## Drawing

The focus of this drawing is on the clock mode, so there are 3 distinct planets
revolving around the central star. When not required, the clock mode can be
toggled off, and the celestial objects go into free orbit with pre-determined
velocities, showing off their majestic trails. When the user wants to read the
time, they can toggle clock mode, though it will take a while before the clock
adjusts the celestial objects back to the proper positions. Not very practical,
but still cool and pleasing to the eyes.


## Primitives and Transformations Used

GL_TRIANGLE_FAN for the circular celestial objects.

glTranslatef, glRotatef. glScalef could have been used for the trails, though it
was easier to just modify the size to be drawn.
glColor3ub is used for hexademical colouring instead of glColor3f.
glColor4f is used for opacity, after converting the values from hexadecimal.


## Methods Modified

- An additional 'r' key resets the view to default.
- Other initialisation methods are slightly modified, such as background color.


## TA Should Know

## Coolest Things

1. The planets leave a trail when not in clock mode. The trail is restarted
   upon leaving clock mode.
2. When switching to clock mode, it is not instantaneous - the celestial objects
   only begin "ticking" when they reach the correct angular position.
3. There is no obvious "ticking" in the clock mode - the celestial objects are
   in "sweep" mode, i.e. seemingly continuous motion instead of distinct ticks,
   replicating certain watches and clocks with higher beats per hour.
4. Each planet has a moon that denotes the smaller timeframe, e.g. the hours
   planet has a moon that follows the minutes, while the seconds planet has a
   milliseconds moon.


