// CS3241Lab2.cpp : Defines the entry point for the console application.
// Sun Wang Jun A0110546R
#include <chrono>
#include <cmath>
#include <iostream>
#include <vector>

/* Include header files depending on platform. */
#ifdef _WIN32
  #include "glut.h"
  #define M_PI 3.14159
#elif __APPLE__
  #include <OpenGL/gl.h>
  #include <GLUT/GLUT.h>
#endif

using namespace std;

float M_DEG_TO_RAD = M_PI / 180;

void drawCircle(float xRadius, float yRadius) {
  glVertex2f(0, 0);
  for (float deg = 0; deg <= 360; deg++) {
    float rad = deg * M_DEG_TO_RAD;
    float x = xRadius * cos(rad);
    float y = yRadius * sin(rad);
    glVertex2f(x, y);
  }
}

class Celestial {
private:
  int trailCount = 0;

  // Draws a trail, limited by the maxTrailCount of the Celestial object.
  void drawTrail(float parentSize) {
    if (inClockMode && trailCount > 0) {
      trailCount--;
    } else if (!inClockMode && trailCount < maxTrailCount) {
      trailCount++;
    }

    float drawSize = size;
    float angleAway = -angularSpeed;
    float transparency = 1.0;

    for (int i = 0; i < trailCount; i++) {
      glPushMatrix();

      glRotatef(angle + angleAway, 0, 0, 1);
      glTranslatef(parentSize + distFromRef, 0, 0);

      glColor4f(r / 255.0, g / 255.0, b / 255.0, transparency);
      glBegin(GL_TRIANGLE_FAN);
      drawCircle(drawSize, drawSize);
      glEnd();
      
      glPopMatrix();

      drawSize = drawSize * 0.9;
      angleAway -= angularSpeed;
      transparency = transparency * 0.9;
    }
  }

  int r, g, b = 0xff;

public:
  bool toClockMode = false;
  bool inClockMode = false;

  float distFromRef = 0;
  float angularSpeed = 0;
  float size = 0;
  float angle = 0;
  int maxTrailCount = 0;

  vector<Celestial*> satellites = vector<Celestial*>();

  Celestial() {}

  Celestial(float distFromRef, float angularSpeed, int rgbColor, float size,
            float angle, int maxTrailCount) {
    this->distFromRef = distFromRef;
    this->angularSpeed = angularSpeed;
    this->size = size;
    this->angle = angle;
    this->maxTrailCount = maxTrailCount;

    this->r = rgbColor / 0x100 / 0x100;
    this->g = rgbColor / 0x100 % 0x100;
    this->b = rgbColor % 0x100;
  }

  // Draws the Celestial object and its children.
  void draw(float parentSize) {
    drawTrail(parentSize);

    glPushMatrix();

    glRotatef(angle, 0, 0, 1);
    glTranslatef(parentSize + distFromRef, 0, 0);

    glColor4ub(r, g, b, 0xff);
    glBegin(GL_TRIANGLE_FAN);
    drawCircle(size, size);
    glEnd();

    glRotatef(-angle, 0, 0, 1);
    for (Celestial *s : satellites) {
      s->draw(size);
    }

    glPopMatrix();
  }

  // Updates the angle of the Celestial object when not in time mode.
  void update() {
    inClockMode = false;
    angle += angularSpeed;
    if (angle < 0.0) {
      angle += 360.0;
    } else if (angle > 360.0) {
      angle -= 360.0;
    }
  }

  // Updates the angle of the Celestial object when in time mode.
  void updateFixed(float fixedAngle) {
    if (toClockMode) {
      // Assuming only clockwise orbits.
      if (angle >= fixedAngle && angle + angularSpeed <= fixedAngle) {
        inClockMode = true;
      } else {
        update();
      }
    }
    if (inClockMode) {
      toClockMode = false;
      angle = fixedAngle;
    }
  }
};


Celestial *star, *seconds, *minutes, *hours;
Celestial *secondsMillis, *minutesSeconds, *hoursMinutes;
vector<Celestial*> objects = vector<Celestial*>();

bool clockMode = false;

void reshape(int w, int h) {
  glViewport(0, 0, (GLsizei) w, (GLsizei) h);

  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();

  glOrtho(-10, 10, -10, 10, -10, 10);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
}

void init(void) {
  glClearColor(0.13, 0.13, 0.13, 1.0);
  glShadeModel(GL_SMOOTH);
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

  // Celestial constructor reference:
  // Celestial(distFromRef, angularSpeed, *color, size, angle, maxTrailCount)

  // Numbers used include 3241 and matric no.

  star = new Celestial(0, 0, 0xFFF9C4, 1, 0, 0);

  seconds = new Celestial(2, -2.32, 0xDCEDC8, 0.4, 32, 30);
  star->satellites.push_back(seconds);

  secondsMillis = new Celestial(0.4, -0.441, 0xB2DFDB, 0.1, 41, 0);
  seconds->satellites.push_back(secondsMillis);

  minutes = new Celestial(5, -1.01, 0xB3E5FC, 0.5, 01, 35);
  star->satellites.push_back(minutes);

  minutesSeconds = new Celestial(0.5, -0.810, 0xC5CAE9, 0.15, 10, 0);
  minutes->satellites.push_back(minutesSeconds);

  hours = new Celestial(8, -0.554, 0xFFCDD2, 0.6, 54, 40);
  star->satellites.push_back(hours);

  hoursMinutes = new Celestial(0.6, -1.606, 0xF8BBD0, 0.2, 06, 0);
  hours->satellites.push_back(hoursMinutes);

  objects.push_back(star);
  objects.push_back(seconds);
  objects.push_back(secondsMillis);
  objects.push_back(minutes);
  objects.push_back(minutesSeconds);
  objects.push_back(hours);
  objects.push_back(hoursMinutes);
}


float alpha = 0.0, k = 1;
float tx = 0.0, ty = 0.0;
void resetSceneValues() {
  alpha = 0.0, k = 1;
  tx = 0.0, ty = 0.0;
}

void display(void) {
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  glPushMatrix();

  glScalef(k, k, k);
  glTranslatef(tx, ty, 0);
  glRotatef(alpha, 0, 0, 1);

  glRotatef(90, 0, 0, 1);
  star->draw(0);

  glPopMatrix();
  glFlush();
}

void idle() {
  if (clockMode) {
    for (Celestial *o : objects) {
      if (!o->inClockMode) {
        o->toClockMode = true;
      }
    }

    chrono::time_point<chrono::system_clock> now = chrono::system_clock::now();
    auto duration = now.time_since_epoch();
    auto millis = chrono::duration_cast<chrono::milliseconds>(duration).count() % 1000;

    time_t currTime = time(NULL);
    struct tm *timeinfo = localtime(&currTime);

    // milliseconds = # of milliseconds / 1000 * 360
    float millisAngle = 360 - (float)millis / 1000 * 360;
    secondsMillis->updateFixed(millisAngle);

    // seconds = (# of milliseconds / 1000) / 60 * 360 = # / 1000 * 6
    float secondsAngle = 360 - (float)(timeinfo->tm_sec * 1000 + millis) * 6 / 1000;
    seconds->updateFixed(secondsAngle);
    minutesSeconds->updateFixed(secondsAngle);

    // minutes = (# of seconds / (60 * 60)) * 360 = # / 10
    float minutesAngle = 360 - (float)(timeinfo->tm_min * 60 + timeinfo->tm_sec) / 10;
    minutes->updateFixed(minutesAngle);
    hoursMinutes->updateFixed(minutesAngle);

    // hours = (# of minutes / 60) / 12 * 360 = # / 2
    int amPmHour = timeinfo->tm_hour % 12;
    float hoursAngle = 360 - (float)(amPmHour * 60 + timeinfo->tm_min) / 2;
    hours->updateFixed(hoursAngle);

  } else {
    for (Celestial *o : objects) {
      o->update();
    }
  }

  glutPostRedisplay();
}

void keyboard(unsigned char key, int x, int y) {
  switch (key) {
    case 'a':
      alpha += 10;
      glutPostRedisplay();
      break;
    case 'd':
      alpha -= 10;
      glutPostRedisplay();
      break;
    case 'q':
      k += 0.1;
      glutPostRedisplay();
      break;
    case 'e':
      if (k > 0.1) { k -= 0.1; }
      glutPostRedisplay();
      break;
    case 'z':
      tx -= 0.1;
      glutPostRedisplay();
      break;
    case 'c':
      tx += 0.1;
      glutPostRedisplay();
      break;
    case 's':
      ty -= 0.1;
      glutPostRedisplay();
      break;
    case 'w':
      ty += 0.1;
      glutPostRedisplay();
      break;
    case 't':
      clockMode = !clockMode;
      break;
    case 'r':
      resetSceneValues();
      glutPostRedisplay();
      break;
    case 27:
      exit(0);
      break;
    default:
      break;
  }
}

int main(int argc, char **argv) {
  cout << "CS3241 Lab 2\n\n";
  cout << "+++++CONTROL BUTTONS+++++++\n\n";
  cout << "Scale Up/Down: Q/E\n";
  cout << "Rotate Clockwise/Counter-clockwise: A/D\n";
  cout << "Move Up/Down: W/S\n";
  cout << "Move Left/Right: Z/C\n";
  cout << "Toggle Clock Mode: T\n";
  cout << "Reset View: R\n";
  cout << "ESC: Quit\n";

  glutInit(&argc, argv);
  glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB | GLUT_DEPTH);
  glutInitWindowSize(600, 600);
  glutInitWindowPosition(50, 50);
  glutCreateWindow(argv[0]);
  init();
  glutDisplayFunc(display);
  glutIdleFunc(idle);
  glutReshapeFunc(reshape);	
  //glutMouseFunc(mouse);
  glutKeyboardFunc(keyboard);
  glutMainLoop();
  
  return 0;
}
